package com.sccci.sccci2014.core;

import android.annotation.TargetApi;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Build;

/**
 * Created by h3r0 on 6/18/14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public abstract class BaseAsyncLoader<E> extends AsyncTaskLoader {

    public BaseAsyncLoader(Context context) {
        super(context);
    }

}

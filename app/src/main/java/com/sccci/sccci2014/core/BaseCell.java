package com.sccci.sccci2014.core;

import android.content.Context;

import com.sccci.sccci2014.util.AppPreferences;

/**
 * Created by h3r0 on 6/30/14.
 */
public class BaseCell {

    private Context mContext;

    private AppPreferences _appPrefs;

    public void setContext(Context mContext){
        this.mContext = mContext;
    }

    public Context getContext(){
        return mContext;
    }

    public AppPreferences getPref(){
        return new AppPreferences(getContext());
    }
}

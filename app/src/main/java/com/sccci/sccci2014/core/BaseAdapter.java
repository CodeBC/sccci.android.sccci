package com.sccci.sccci2014.core;

import android.content.Context;
import android.view.LayoutInflater;

import java.util.ArrayList;

/**
 * Created by h3r0 on 6/17/14.
 */
public abstract class BaseAdapter<E> extends android.widget.BaseAdapter {

    protected ArrayList<E> objects = new ArrayList<E>();

    protected Context mContext;
    protected LayoutInflater inflater;

    protected BaseAdapter(Context mContext) {
        this.mContext = mContext;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<E> objects) {
        this.objects = objects;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}

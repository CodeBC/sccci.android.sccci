package com.sccci.sccci2014.core;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.sccci.sccci2014.util.AppPreferences;

/**
 * Created by h3r0 on 5/29/14.
 */
public class BaseFragment extends Fragment {

    public AppPreferences _appPrefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _appPrefs = new AppPreferences(getActivity().getApplicationContext());
    }

}

package com.sccci.sccci2014.core;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.GalleryPagerAdapter;
import com.sccci.sccci2014.model.GridItem;
import com.sccci.sccci2014.util.AppPreferences;

import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by h3r0 on 5/29/14.
 */
public class BaseFragmentActivity extends FragmentActivity{

    @Optional
    @InjectView(R.id.top_banner)
    public ViewPager top_banner;

    private long ANIM_VIEWPAGER_DELAY = 5000;

    private Handler topBannerHandler = new Handler();
    private int top = 0;
    private Runnable topBannerRunner = new Runnable() {
        public void run() {
            if(top_banner != null){
                if (top_banner.getAdapter().getCount() > 1) {
                    if (top >= 4) {
                        top = 0;
                    } else {
                        top = top + 1;
                    }
                    top_banner.setCurrentItem(top, true);
                    topBannerHandler.postDelayed(topBannerRunner, ANIM_VIEWPAGER_DELAY);
                }
            }
        }
    };

    public AppPreferences _appPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(0,0);

        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        _appPrefs = new AppPreferences(getApplicationContext());
    }

    public void setTitle(){
        GridItem object = (GridItem) getIntent().getExtras().get("object");

        if(_appPrefs.isLangMandarin())
            getActionBar().setTitle(object.cn_name);
        else
            getActionBar().setTitle(object.en_name);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }

    protected void generateTopBanner() {
        int[] topImages = {R.drawable.anz_baner,R.drawable.dbs_banner,R.drawable.deskera_banner,R.drawable.ocbc_banner,R.drawable.rhb_banner};
        String[] links = {"http://www.anz.com.sg","http://www.dbs.com.sg/sme","http://www.deskera.sg","http://www.ocbc.com.sg/business-banking/","http://www.rhbbank.com.sg/bizpower/quad.html "};
        GalleryPagerAdapter topBarAdapter = new GalleryPagerAdapter(this, topImages, links);
        top_banner.setAdapter(topBarAdapter);
        top_banner.setOnTouchListener(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (topBannerHandler != null) {
            topBannerHandler.removeCallbacks(topBannerRunner);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        topBannerHandler.postDelayed(topBannerRunner, ANIM_VIEWPAGER_DELAY);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

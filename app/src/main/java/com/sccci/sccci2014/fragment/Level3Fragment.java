package com.sccci.sccci2014.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.widget.TouchImageView;

/**
 * Created by h3r0 on 6/26/14.
 */
public class Level3Fragment extends Fragment{

    public static Level3Fragment create() {
        Level3Fragment fragment = new Level3Fragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        View view = (RelativeLayout) inflater.inflate(R.layout.fragment_level_three, container, false);

        TouchImageView imageView = (TouchImageView) view.findViewById(R.id.img_map);

        return view;
    }
}

package com.sccci.sccci2014.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.ui.Map;

/**
 * Created by h3r0 on 6/26/14.
 */
public class MapFragment extends Fragment {

    private static View view;

    private static GoogleMap mMap;
    private static Double latitude, longitude;

    FragmentManager fragmentManager;

    public static MapFragment create() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentManager = Map.fragmentManager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = (RelativeLayout) inflater.inflate(R.layout.fragment_map, container, false);
        latitude = 1.293640;
        longitude = 103.857326;

        RelativeLayout getdir = (RelativeLayout) view.findViewById(R.id.getdir);
        getdir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?f=d&daddr=1.293640,103.857326"));
                startActivity(intent);
            }
        });

        setUpMapIfNeeded();

        return view;
    }

    void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) fragmentManager.findFragmentById(R.id.location_map)).getMap();
            if (mMap != null) setUpMap();
        }
    }

    void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (mMap != null)
            setUpMap();

        if (mMap == null) {
            mMap = ((SupportMapFragment) fragmentManager.findFragmentById(R.id.location_map)).getMap();
            if (mMap != null)
                setUpMap();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mMap != null) {
            if(!getActivity().isDestroyed()){
                fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(R.id.location_map)).commitAllowingStateLoss();
            }
            mMap = null;
        }
    }
}


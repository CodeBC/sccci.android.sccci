package com.sccci.sccci2014.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.ProgrammeListAdapter;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjEvent;
import com.sccci.sccci2014.ui.ProgrammeDetail;
import com.sccci.sccci2014.util.L;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by h3r0 on 6/25/14.
 */
public class ProgrammeFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    private ObjEvent obj;

    StickyListHeadersListView list;

    ArrayList<ObjEvent> objects = new ArrayList<ObjEvent>();
    Dao<ObjEvent, Integer> dao;
    boolean isMySchedule;

    public static ProgrammeFragment create(ObjEvent obj, boolean isMySchedule) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_PAGE, obj);
        args.putBoolean("isMySchedule", isMySchedule);
        ProgrammeFragment fragment = new ProgrammeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        obj = (ObjEvent) getArguments().get(ARG_PAGE);
        isMySchedule = getArguments().getBoolean("isMySchedule");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        list = (StickyListHeadersListView) view.findViewById(R.id.list);

        try {
            dao = OpenHelperManager.getHelper(getActivity().getApplicationContext(), DatabaseHelper.class).getProgrammerDAO();
            if (isMySchedule) {
                objects = (ArrayList<ObjEvent>) dao.queryBuilder()
                        .orderBy("event_id", true)
                        .orderBy("time_name", false)
                        .orderBy("event_name", true)
                        .orderBy("start_time", true)
                        .where().eq("day_name", obj.day_name).and().eq("is_saved", true)
                        .query();
            } else {
                objects = (ArrayList<ObjEvent>) dao.queryBuilder()
                        .orderBy("event_id", true)
                        .orderBy("time_name", false)
                        .orderBy("event_name", true)
                        .orderBy("start_time", true)
                        .where().eq("day_name", obj.day_name)
                        .query();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Collections.sort(objects,new Comparator<ObjEvent>() {
            @Override
            public int compare(ObjEvent lhs, ObjEvent rhs) {
                int lhs_hour = Integer.parseInt(lhs.start_time.split(":")[0]);
                int rhs_hour = Integer.parseInt(rhs.start_time.split(":")[0]);
                return lhs_hour - rhs_hour;
            }
        });

        ProgrammeListAdapter adapter = new ProgrammeListAdapter(getActivity().getApplicationContext(), dao);
        adapter.setData(objects);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                L.i("Total Speaker : " + objects.get(position).speakers.size());
                Intent detail = new Intent(getActivity().getApplicationContext(), ProgrammeDetail.class);
                detail.putExtra("object",objects.get(position));
                startActivity(detail);
            }
        });

        return view;
    }

}


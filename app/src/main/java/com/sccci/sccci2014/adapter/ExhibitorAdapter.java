package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjExhibitor;
import com.sccci.sccci2014.ui.view.ExhibitorCell;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorAdapter extends BaseAdapter {

    public ExhibitorAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ExhibitorCell holder;

        if (convertView != null) {
            holder = (ExhibitorCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_exhibitor, null, false);
            holder = new ExhibitorCell(convertView);
            convertView.setTag(holder);
        }

        holder.bind((ObjExhibitor) objects.get(position));

        return convertView;
    }

}

package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjExhibitor;
import com.sccci.sccci2014.ui.view.ExhibitorListCell;
import com.sccci.sccci2014.ui.view.ExhibitorListCellHeader;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorListAdapter extends BaseAdapter implements StickyListHeadersAdapter{

    public ExhibitorListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ExhibitorListCell holder;

        if (convertView != null) {
            holder = (ExhibitorListCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_exhibitorlist, null, false);
            holder = new ExhibitorListCell(convertView);
            convertView.setTag(holder);
        }

        holder.setContext(mContext);
        holder.bind((ObjExhibitor) objects.get(position));

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        ExhibitorListCellHeader holder;

        if (convertView != null) {
            holder = (ExhibitorListCellHeader) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_exhibitor_title, null, false);
            holder = new ExhibitorListCellHeader(convertView);
            convertView.setTag(holder);
        }

        holder.bind((ObjExhibitor) objects.get(position));

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {

        if(((ObjExhibitor) objects.get(position)).exhibitor_type.equalsIgnoreCase("Sponsor")){
            return ((ObjExhibitor) objects.get(position)).exhibitor_category.hashCode();
        }
        return ((ObjExhibitor) objects.get(position)).exhibitor_name.subSequence(0, 1).charAt(0);
    }
}

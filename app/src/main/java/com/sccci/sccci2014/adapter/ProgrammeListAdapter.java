package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjEvent;
import com.sccci.sccci2014.ui.view.ProgrammeListCell;
import com.sccci.sccci2014.ui.view.ProgrammeListHeader;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by h3r0 on 6/25/14.
 */
public class ProgrammeListAdapter extends BaseAdapter implements StickyListHeadersAdapter{

    Dao<ObjEvent, Integer> dao;

    public ProgrammeListAdapter(Context mContext,Dao<ObjEvent, Integer> dao) {
        super(mContext);
        this.dao = dao;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ProgrammeListCell holder;

        if (convertView != null) {
            holder = (ProgrammeListCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_programme, null, false);
            holder = new ProgrammeListCell(convertView);
            convertView.setTag(holder);
        }
        holder.setContext(mContext);
        holder.bind((ObjEvent) objects.get(position),dao);

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        ProgrammeListHeader holder;

        if (convertView != null) {
            holder = (ProgrammeListHeader) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_programme_title, null, false);
            holder = new ProgrammeListHeader(convertView);
            convertView.setTag(holder);
        }

        holder.bind((ObjEvent) objects.get(position));

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return ((ObjEvent) objects.get(position)).time_name.hashCode();
    }
}

package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.GridItem;
import com.sccci.sccci2014.ui.view.GridCell;

/**
 * Created by h3r0 on 6/18/14.
 */
public class HomeGridAdapter extends BaseAdapter {

    public HomeGridAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GridCell holder;

        if (convertView != null) {
            holder = (GridCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.griditem_home, null, false);
            holder = new GridCell(convertView);
            convertView.setTag(holder);
        }

        holder.setContext(mContext);
        holder.bind((GridItem) objects.get(position));

        return convertView;
    }

}

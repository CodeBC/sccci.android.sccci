package com.sccci.sccci2014.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sccci.sccci2014.fragment.ProgrammeFragment;
import com.sccci.sccci2014.model.ObjEvent;

import java.util.ArrayList;

/**
 * Created by h3r0 on 6/25/14.
 */
public class ProgrammeFragmentAdapter extends FragmentPagerAdapter {

    ArrayList<ObjEvent> objects = new ArrayList<ObjEvent>();
    boolean isMySchedule;

    public ProgrammeFragmentAdapter(FragmentManager fragmentManager, ArrayList<ObjEvent> objects) {
        this(fragmentManager,objects,false);
    }

    public ProgrammeFragmentAdapter(FragmentManager fragmentManager, ArrayList<ObjEvent> objects,boolean isMySchedule) {
        super(fragmentManager);
        this.objects = objects;
        this.isMySchedule = isMySchedule;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Fragment getItem(int position) {
        return ProgrammeFragment.create(objects.get(position),isMySchedule);
    }

    public CharSequence getPageTitle(int position) {
        return "" + objects.get(position).day_name;
    }
}
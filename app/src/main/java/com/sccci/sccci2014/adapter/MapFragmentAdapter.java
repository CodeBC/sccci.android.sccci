package com.sccci.sccci2014.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sccci.sccci2014.fragment.Level3Fragment;
import com.sccci.sccci2014.fragment.Level4Fragment;
import com.sccci.sccci2014.fragment.MapFragment;

/**
 * Created by h3r0 on 6/26/14.
 */
public class MapFragmentAdapter extends FragmentPagerAdapter {

    public MapFragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new MapFragment();
            case 1:
                return Level3Fragment.create();
            case 2:
                return Level4Fragment.create();
            default:
                return Level3Fragment.create();
        }
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Venue";
            case 1:
                return "Level 3";
            case 2:
                return "Level 4";
            default:
                return "";
        }
    }
}

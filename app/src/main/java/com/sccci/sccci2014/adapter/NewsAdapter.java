package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjAbout;
import com.sccci.sccci2014.model.ObjNews;
import com.sccci.sccci2014.ui.view.NewsListCell;

/**
 * Created by h3r0 on 6/18/14.
 */
public class NewsAdapter extends BaseAdapter {

    public NewsAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NewsListCell holder;

        if (convertView != null) {
            holder = (NewsListCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_info, null, false);
            holder = new NewsListCell(convertView);
            convertView.setTag(holder);
        }

        holder.setContext(mContext);
        holder.bind((ObjNews) objects.get(position));

        return convertView;
    }

}

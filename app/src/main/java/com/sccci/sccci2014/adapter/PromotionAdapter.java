package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjPromotion;
import com.sccci.sccci2014.ui.view.PromotionListCell;

/**
 * Created by h3r0 on 6/18/14.
 */
public class PromotionAdapter extends BaseAdapter {

    public PromotionAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PromotionListCell holder;

        if (convertView != null) {
            holder = (PromotionListCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_promotion, null, false);
            holder = new PromotionListCell(convertView);
            convertView.setTag(holder);
        }

        holder.setContext(mContext);
        holder.bind((ObjPromotion) objects.get(position));

        return convertView;
    }

}

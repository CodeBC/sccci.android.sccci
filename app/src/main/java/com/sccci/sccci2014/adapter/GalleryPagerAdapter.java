package com.sccci.sccci2014.adapter;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.sccci.sccci2014.util.SystemUtils;

public class GalleryPagerAdapter extends PagerAdapter {

    private Activity activity;
    private int[] drawableIDs;
    private String[] links;

    public GalleryPagerAdapter(Activity activity, int[] drawableIDs) {
        this.activity = activity;
        this.drawableIDs = drawableIDs;
    }

    public GalleryPagerAdapter(Activity activity, int[] drawableIDs, String[] links) {
        this.activity = activity;
        this.drawableIDs = drawableIDs;
        this.links = links;
    }

    @Override
    public int getCount() {
        return drawableIDs.length;
    }

    @Override
    public Object instantiateItem(View collection, final int position) {
        ImageView imageView = new ImageView(activity);
        imageView.setBackgroundResource(drawableIDs[position]);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.OpenBrowser(activity.getApplicationContext(),links[position]);
            }
        });
        ((ViewPager) collection).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((ImageView) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @Override
    public void finishUpdate(View arg0) {
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void startUpdate(View arg0) {
    }
}  
package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjSpeaker;
import com.sccci.sccci2014.ui.view.SpeakerListCell;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SpeakerAdapter extends BaseAdapter {

    public SpeakerAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SpeakerListCell holder;

        if (convertView != null) {
            holder = (SpeakerListCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_info, null, false);
            holder = new SpeakerListCell(convertView);
            convertView.setTag(holder);
        }

        holder.setContext(mContext);
        holder.bind((ObjSpeaker) objects.get(position));

        return convertView;
    }

}

package com.sccci.sccci2014.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseAdapter;
import com.sccci.sccci2014.model.ObjSponsor;
import com.sccci.sccci2014.ui.view.SponsorListCell;
import com.sccci.sccci2014.ui.view.SponsorListHeader;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SponsorAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    public SponsorAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SponsorListCell holder;

        if (convertView != null) {
            holder = (SponsorListCell) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_sponsor, null, false);
            holder = new SponsorListCell(convertView);
            convertView.setTag(holder);
        }

        holder.setContext(mContext);
        holder.bind((ObjSponsor) objects.get(position));

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        SponsorListHeader holder;

        if (convertView != null) {
            holder = (SponsorListHeader) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.listitem_sponsor_title, null, false);
            holder = new SponsorListHeader(convertView);
            convertView.setTag(holder);
        }

        holder.bind((ObjSponsor) objects.get(position));

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return ((ObjSponsor) objects.get(position)).exhibitor_category.hashCode();
    }
}

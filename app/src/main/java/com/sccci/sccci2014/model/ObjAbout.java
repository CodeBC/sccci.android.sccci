package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by h3r0 on 6/18/14.
 */
@DatabaseTable(tableName = "tbl_about")
public class ObjAbout implements Serializable {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField
    public String title;

    @DatabaseField
    public String title_cn;

    @DatabaseField
    public String description;

    @DatabaseField
    public String description_cn;

}

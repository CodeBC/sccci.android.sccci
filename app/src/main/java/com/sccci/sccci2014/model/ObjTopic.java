package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;

@DatabaseTable(tableName = "tbl_topic")
public class ObjTopic implements Serializable{

    @DatabaseField(id = true)
    public int topic_id;

    @DatabaseField
    public int event_id;

    @DatabaseField
    public String topic_name;

    @DatabaseField
    public int order;

    public ArrayList<ObjSpeaker> speaker_array;
}

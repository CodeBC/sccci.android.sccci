package com.sccci.sccci2014.model;

import java.io.Serializable;

public class GridItem implements Serializable{

	public String en_name;

    public String cn_name;

	public int thumb;

    public Class<?> cls;

    public GridItem() {
    }

    public GridItem(String en_name, int thumb, Class<?> cls) {
        this.en_name = en_name;
        this.thumb = thumb;

        this.cls = cls;
    }

    public GridItem(String en_name, String cn_name, int thumb, Class<?> cls) {

        this.en_name = en_name;
        this.cn_name = cn_name;
        this.thumb = thumb;
        this.cls = cls;
    }
}

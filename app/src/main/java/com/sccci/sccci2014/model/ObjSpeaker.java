package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tbl_speaker")
public class ObjSpeaker implements Serializable{

    @DatabaseField(id = true)
    public int speaker_id;

    @DatabaseField
    public String speaker_name;

    @DatabaseField
    public String speaker_name_cn;

    @DatabaseField
    public String speaker_company;

    @DatabaseField
    public String speaker_company_cn;

    @DatabaseField
    public String speaker_designation;

    @DatabaseField
    public String speaker_designation_cn;

    @DatabaseField
    public String speaker_description;

    @DatabaseField
    public String speaker_description_cn;

    @DatabaseField
    public String speaker_avatar;

}

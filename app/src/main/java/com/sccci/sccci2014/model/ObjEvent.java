package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;

@DatabaseTable(tableName = "tbl_event")
public class ObjEvent implements Serializable{

    @DatabaseField(id = true)
	public int event_id;

    @DatabaseField
    public String day_name;

    @DatabaseField
    public String time_name;

    @DatabaseField
    public String start_time;

    @DatabaseField
    public String end_time;

    @DatabaseField
    public String event_name_cn;

    @DatabaseField
    public String event_name;

    @DatabaseField
    public String event_description_cn;

    @DatabaseField
    public String event_description;

    @DatabaseField
    public String event_language;

    @DatabaseField
    public String event_location;

    @DatabaseField
    public boolean is_saved = false;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    public ArrayList<Integer> speakers;

}

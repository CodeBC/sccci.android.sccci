package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tbl_exhibitor")
public class ObjExhibitor implements Serializable {

    @DatabaseField(id = true)
    public int exhibitor_id;

    @DatabaseField
    public String exhibitor_name;

    @DatabaseField
    public String exhibitor_name_cn;

    @DatabaseField
    public String exhibitor_type;

    @DatabaseField
    public String exhibitor_category;

    @DatabaseField
    public String exhibitor_about;

    @DatabaseField
    public String exhibitor_about_cn;

    @DatabaseField
    public String exhibitor_contact;

    @DatabaseField
    public String exhibitor_contact_cn;

    @DatabaseField
    public String exhibitor_logo;

    @DatabaseField
    public String exhibitor_booth;
}

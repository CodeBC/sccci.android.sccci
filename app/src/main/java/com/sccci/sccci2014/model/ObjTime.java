package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;

@DatabaseTable(tableName = "tbl_time")
public class ObjTime implements Serializable{

    @DatabaseField(id = true)
    public int time_id;

    @DatabaseField
    public String time_name;

    public ArrayList<ObjEvent> events_array;
}

package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by h3r0 on 6/18/14.
 */
@DatabaseTable(tableName = "tbl_news")
public class ObjNews implements Serializable {

    @DatabaseField(id = true)
    public int news_id;

    @DatabaseField
    public String news_title;

    @DatabaseField
    public String news_title_cn;

    @DatabaseField
    public String news_description;

    @DatabaseField
    public String news_description_cn;

}

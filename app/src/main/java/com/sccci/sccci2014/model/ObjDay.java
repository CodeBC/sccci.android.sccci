package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;

@DatabaseTable(tableName = "tbl_day")
public class ObjDay implements Serializable {

    @DatabaseField(id = true)
    public int day_id;

    @DatabaseField
    public String day_name;

    public ArrayList<ObjTime> time_slots;
}

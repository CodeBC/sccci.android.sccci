package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = "tbl_location")
public class ObjLocation implements Serializable {

    @DatabaseField(id = true)
    public int location_id;

    @DatabaseField
    public String location_name;

}

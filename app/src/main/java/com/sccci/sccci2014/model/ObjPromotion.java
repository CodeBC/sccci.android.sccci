package com.sccci.sccci2014.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by h3r0 on 6/21/14.
 */
@DatabaseTable(tableName = "tbl_promotion")
public class ObjPromotion implements Serializable {

    @DatabaseField(id = true)
    public int promotion_id;

    @DatabaseField
    public String promotion_title;

    @DatabaseField
    public String promotion_title_cn;

    @DatabaseField
    public String promotion_logo;

    @DatabaseField
    public String promotion_description;

    @DatabaseField
    public String promotion_description_cn;

    @DatabaseField
    public String promotion_link;

    @DatabaseField
    public String promotion_map;

    @DatabaseField
    public String promotion_excerpt;

    @DatabaseField
    public String promotion_excerpt_cn;

}

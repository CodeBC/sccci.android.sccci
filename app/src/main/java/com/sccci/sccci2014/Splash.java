package com.sccci.sccci2014;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.sccci.sccci2014.api.AboutLoader;
import com.sccci.sccci2014.api.ClinicLoader;
import com.sccci.sccci2014.api.ExhibitorLoader;
import com.sccci.sccci2014.api.NewsLoader;
import com.sccci.sccci2014.api.ProgrammeLoader;
import com.sccci.sccci2014.api.PromotionLoader;
import com.sccci.sccci2014.api.SpeakerLoader;
import com.sccci.sccci2014.api.SponsorLoader;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.ui.Home;
import com.sccci.sccci2014.util.Constants;

/**
 * Created by h3r0 on 6/12/14.
 */
public class Splash extends BaseActivity implements LoaderManager.LoaderCallbacks<String> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        if (_appPrefs.isFirstTime()) {

            _appPrefs.setFirstTime(false);
            _appPrefs.setNotify(true);
            _appPrefs.setLang(false);

        }

        _appPrefs.setAboutLoaded(false);
        _appPrefs.setClinicLoaded(false);
        _appPrefs.setExhibitorsLoaded(false);
        _appPrefs.setNewsLoaded(false);
        _appPrefs.setProgrammeLoaded(false);
        _appPrefs.setPromotionLoaded(false);
        _appPrefs.setSpeakersLoaded(false);
        _appPrefs.setSponsorsLoaded(false);

        getSupportLoaderManager().initLoader(0, null, this).forceLoad();
        getSupportLoaderManager().initLoader(1, null, this).forceLoad();
        getSupportLoaderManager().initLoader(2, null, this).forceLoad();
        getSupportLoaderManager().initLoader(3, null, this).forceLoad();
        getSupportLoaderManager().initLoader(4, null, this).forceLoad();
        getSupportLoaderManager().initLoader(5, null, this).forceLoad();
        getSupportLoaderManager().initLoader(6, null, this).forceLoad();
        getSupportLoaderManager().initLoader(7, null, this).forceLoad();

    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        if (id == 0) return new AboutLoader(this);
        if (id == 1) return new ExhibitorLoader(this);
        if (id == 2) return new NewsLoader(this);
        if (id == 3) return new ProgrammeLoader(this);
        if (id == 4) return new PromotionLoader(this);
        if (id == 5) return new SpeakerLoader(this);
        if (id == 6) return new SponsorLoader(this);
        if (id == 7) return new ClinicLoader(this);
        return null;
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        if (loader.getId() == 0 && checkReturn(data)) _appPrefs.setAboutLoaded(true);
        if (loader.getId() == 1 && checkReturn(data)) _appPrefs.setExhibitorsLoaded(true);
        if (loader.getId() == 2 && checkReturn(data)) _appPrefs.setNewsLoaded(true);
        if (loader.getId() == 3 && checkReturn(data)) _appPrefs.setProgrammeLoaded(true);
        if (loader.getId() == 4 && checkReturn(data)) _appPrefs.setPromotionLoaded(true);
        if (loader.getId() == 5 && checkReturn(data)) _appPrefs.setSpeakersLoaded(true);
        if (loader.getId() == 6 && checkReturn(data)) _appPrefs.setSponsorsLoaded(true);
        if (loader.getId() == 7 && checkReturn(data)) _appPrefs.setClinicLoaded(true);

        if (_appPrefs.isAboutLoaded()
                && _appPrefs.isExhibitorsLoaded()
                && _appPrefs.isNewsLoaded()
                && _appPrefs.isProgrammeLoaded()
                && _appPrefs.isPromotionLoaded()
                && _appPrefs.isSpeakersLoaded()
                && _appPrefs.isSponsorsLoaded()
                && _appPrefs.isClinicLoaded()) {
            finish();
            startActivity(new Intent(getApplicationContext(), Home.class));
        }
    }

    public boolean checkReturn(String data) {
        if (data.equalsIgnoreCase(Constants.RETURN_SUCCESS))
            return true;
        return true;
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }
}

package com.sccci.sccci2014.api;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.koushikdutta.ion.Ion;
import com.sccci.sccci2014.util.AppPreferences;
import com.sccci.sccci2014.util.Constants;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ClinicLoader extends AsyncTaskLoader<String> {

    AppPreferences _appPrefs;

    public ClinicLoader(Context context) {
        super(context);

        _appPrefs = new AppPreferences(getContext());
    }

    @Override
    public String loadInBackground() {

        try {
            String result = Ion.with(getContext())
                    .load(Constants.URL_CLINIC)
                    .setLogging(getClass().getSimpleName(), Log.DEBUG)
                    .asString().get();

            JsonParser parser = new JsonParser();

            InputStream in = new ByteArrayInputStream(result.getBytes("UTF-8"));

            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.setLenient(true);

            JsonArray jarr = (JsonArray) parser.parse(reader);

            Gson gson = new GsonBuilder().create();

            for (int i = 0; i < jarr.size(); i++) {
                JsonObject jobj = jarr.get(i).getAsJsonObject();

                _appPrefs.setPC(jobj.get("partners_clinic").getAsString());
                _appPrefs.setPCCN(jobj.get("partners_clinic_cn").getAsString());
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        }

        return Constants.RETURN_SUCCESS;
    }

}

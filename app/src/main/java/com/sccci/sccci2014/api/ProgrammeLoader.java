package com.sccci.sccci2014.api;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.koushikdutta.ion.Ion;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjEvent;
import com.sccci.sccci2014.util.AppPreferences;
import com.sccci.sccci2014.util.Constants;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ProgrammeLoader extends AsyncTaskLoader<String> {

    AppPreferences _appPrefs;

    private Dao<ObjEvent,Integer> programmeDAO;

    public ProgrammeLoader(Context context) {
        super(context);

        try {
            programmeDAO = OpenHelperManager.getHelper(getContext(), DatabaseHelper.class).getProgrammerDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        _appPrefs = new AppPreferences(getContext());
    }

    @Override
    public String loadInBackground() {

        try {
            String result = Ion.with(getContext())
                    .load(Constants.URL_PROGRAMME)
                    .setLogging(getClass().getSimpleName(), Log.DEBUG)
                    .asString().get();

            JsonParser parser = new JsonParser();

            InputStream in = new ByteArrayInputStream(result.getBytes("UTF-8"));

            JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
            reader.setLenient(true);

            JsonArray jarr = (JsonArray) parser.parse(reader);

            Gson gson = new GsonBuilder().create();

            for(int i=0;i<jarr.size();i++){
                JsonObject jobj = jarr.get(i).getAsJsonObject();

                ObjEvent obj = gson.fromJson(jobj,ObjEvent.class);

                programmeDAO.createOrUpdate(obj);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        } catch (SQLException e) {
            e.printStackTrace();
            return Constants.RETURN_FAIL;
        }

        return Constants.RETURN_SUCCESS;
    }

}


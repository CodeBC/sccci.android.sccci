package com.sccci.sccci2014.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

/**
 * Created by h3r0 on 6/17/14.
 */
public class SystemUtils {

    /**
     * Get Device Unique ID
     *
     * @param mContext Application Context
     * @return Device UDID
     */
    public static String udid(Context mContext) {
        String udid = android.provider.Settings.System.getString(mContext.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        return udid;
    }

    /**
     * Call Phone
     *
     * @param number
     * @return
     */
    public static void PhoneCall(Context mContext, String number) {
        if (wasPermissionGranted(mContext, Manifest.permission.CALL_PHONE)) {
            Intent call = new Intent(Intent.ACTION_DIAL);
            call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            call.setData(Uri.parse("tel:" + number));
            mContext.startActivity(call);
        } else {
            L.e("You need to add " + Manifest.permission.CALL_PHONE + " permission.");
        }
    }

    /**
     * Send Email
     *
     * @param mail address
     * @return
     */
    public static void SendEmail(Context mContext, String mail) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        email.setType("text/plain");
        email.putExtra(Intent.EXTRA_EMAIL, mail);
        email.putExtra(Intent.EXTRA_SUBJECT, "");
        email.putExtra(Intent.EXTRA_TEXT, "");

        mContext.startActivity(Intent.createChooser(email, "Send Email"));
    }

    /**
     * Open URL in Browser
     *
     * @param link to open in browser
     * @return
     */
    public static void OpenBrowser(Context mContext, String link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(browserIntent);
    }

    /**
     * Check whether or not permission is granted
     *
     * @param permission
     * @return boolean
     */
    private static boolean wasPermissionGranted(Context context, String permission) {
        PackageManager pm = context.getPackageManager();
        int hasPerm = pm.checkPermission(permission, context.getPackageName());
        return (hasPerm == PackageManager.PERMISSION_GRANTED);
    }
}

package com.sccci.sccci2014.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by h3r0 on 5/30/14.
 */
public class AppPreferences {

    private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName(); //  Name of the file -.xml
    private SharedPreferences _sharedPrefs;
    private SharedPreferences.Editor _prefsEditor;

    //Saved Data
    public static final String key_isFirstTime = "isFirstTime";
    public static final String key_lang = "language";
    public static final String key_notify = "notify";

    public static final String key_partnerclinic = "pc";
    public static final String key_partnerclinic_cn = "pc_cn";

    public static final String key_isAboutLoaded = "isAboutLoaded";
    public static final String key_isClinicLoaded = "isClinicLoaded";
    public static final String key_isExhibitorLoaded = "isExhibitorLoaded";
    public static final String key_isNewsLoaded = "isNewsLoaded";
    public static final String key_isProgrammeLoaded = "isProgrammeLoaded";
    public static final String key_isPromoLoaded = "isPromotionLoaded";
    public static final String key_isSpeakersLoaded = "isSpeakersLoaded";
    public static final String key_isSponsorLoaded = "isSponsorsLoaded";


    public AppPreferences(Context context) {
        this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
        this._prefsEditor = _sharedPrefs.edit();
    }

    public boolean isFirstTime() {
        return _sharedPrefs.getBoolean(key_isFirstTime, true);
    }

    public boolean isNotificationEnabled() {
        return _sharedPrefs.getBoolean(key_notify, true);
    }

    public boolean isLangMandarin() {
        return _sharedPrefs.getBoolean(key_lang, false);
    }

    public void setFirstTime(boolean isFirstTime) {
        _prefsEditor.putBoolean(key_isFirstTime, isFirstTime);
        _prefsEditor.commit();
    }

    public void setLang(boolean isLang) {
        _prefsEditor.putBoolean(key_lang, isLang);
        _prefsEditor.commit();
    }

    public void setNotify(boolean isNotify) {
        _prefsEditor.putBoolean(key_notify, isNotify);
        _prefsEditor.commit();
    }

    public void setPC(String pc){
        _prefsEditor.putString(key_partnerclinic,pc);
        _prefsEditor.commit();
    }

    public String getPC(){
        return _sharedPrefs.getString(key_partnerclinic,"");
    }

    public void setPCCN(String pc){
        _prefsEditor.putString(key_partnerclinic_cn,pc);
        _prefsEditor.commit();
    }

    public String getPCCN(){
        return _sharedPrefs.getString(key_partnerclinic_cn,"");
    }

    public boolean isAboutLoaded() {
        return _sharedPrefs.getBoolean(key_isAboutLoaded, false);
    }

    public void setAboutLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isAboutLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isClinicLoaded() {
        return _sharedPrefs.getBoolean(key_isClinicLoaded, false);
    }

    public void setClinicLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isClinicLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isExhibitorsLoaded() {
        return _sharedPrefs.getBoolean(key_isExhibitorLoaded, false);
    }

    public void setExhibitorsLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isExhibitorLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isNewsLoaded() {
        return _sharedPrefs.getBoolean(key_isNewsLoaded, false);
    }

    public void setNewsLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isNewsLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isProgrammeLoaded() {
        return _sharedPrefs.getBoolean(key_isProgrammeLoaded, false);
    }

    public void setProgrammeLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isProgrammeLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isPromotionLoaded(){
        return _sharedPrefs.getBoolean(key_isPromoLoaded,false);
    }

    public void setPromotionLoaded(boolean isLoaded){
        _prefsEditor.putBoolean(key_isPromoLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isSpeakersLoaded() {
        return _sharedPrefs.getBoolean(key_isSpeakersLoaded, false);
    }

    public void setSpeakersLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isSpeakersLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public boolean isSponsorsLoaded() {
        return _sharedPrefs.getBoolean(key_isSponsorLoaded, false);
    }

    public void setSponsorsLoaded(boolean isLoaded) {
        _prefsEditor.putBoolean(key_isSponsorLoaded, isLoaded);
        _prefsEditor.commit();
    }

    public void clear() {
        _prefsEditor.clear();
        _prefsEditor.commit();
    }

}

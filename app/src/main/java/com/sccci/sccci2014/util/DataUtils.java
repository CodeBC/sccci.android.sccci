package com.sccci.sccci2014.util;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by h3r0 on 6/2/14.
 */
public class DataUtils {

    /**
     * Read Text from assets folder and return String
     *
     * @param mContext Application Context
     * @param filename of the file to open (under assets folder)
     * @return String value of requested file
     */
    public static String loadTextFromAssets(Context mContext, String filename) {
        try {
            InputStream is = mContext.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Open a private object associated with this Context's application package for writing. Creates the file if it doesn't already exist.
     *
     * @param mContext Application Context
     * @param filename of the file to open (under assets folder)
     * @param file     Object
     * @return
     */
    public static void WriteObject(Context mContext, String filename, Object file) {
        try {
            FileOutputStream fos = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(file);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open a private object associated with this Context's application package for reading.
     *
     * @param mContext Application Context
     * @param filename of the file to open (under assets folder)
     * @return Object
     */
    public static Object ReadObject(Context mContext, String filename) {
        try {
            FileInputStream fis = mContext.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = (Object) ois.readObject();
            fis.close();
            return obj;

        } catch (Exception e) {
            e.printStackTrace();
            return new Object();
        }
    }

    /**
     * Open a private file associated with this Context's application package for writing. Creates the file if it doesn't already exist.
     *
     * @param mContext Application Context
     * @param filename  name of the file to write
     * @param arraylist to write
     * @return
     */
    public static <E> void WriteArraylist(Context mContext, String filename, ArrayList<E> arraylist) {
        try {

            L.i(mContext.getFileStreamPath(filename).getAbsolutePath());

            FileOutputStream fos = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(arraylist);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open a private file associated with this Context's application package for reading.
     *
     * @param filename of the file to open
     * @return Object
     */
    public static Object ReadArraylist(Context mContext, String filename) {
        try {
            FileInputStream fis = mContext.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object obj = (Object) ois.readObject();
            fis.close();
            return obj;

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Object>();
        }
    }
}

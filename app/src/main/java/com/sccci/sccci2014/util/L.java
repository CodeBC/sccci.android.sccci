package com.sccci.sccci2014.util;

import android.util.Log;

/**
 * App log logger
 *
 * @author Hein Win Toe (hein[at]nexlabs[dot]co)
 * @since 1.0.0
 */

public class L {

    public static void e(final String msg) {

        final Throwable t = new Throwable();
        final StackTraceElement[] elements = t.getStackTrace();

        final String callerClassName = elements[1].getClassName();
        final String callerMethodName = elements[1].getMethodName();

        Log.e(callerClassName, "[" + callerMethodName + "] " + msg);
    }

    public static void i(final String msg) {

        final Throwable t = new Throwable();
        final StackTraceElement[] elements = t.getStackTrace();

        final String callerClassName = elements[1].getClassName();
        final String callerMethodName = elements[1].getMethodName();

        Log.i(callerClassName, "[" + callerMethodName + "] " + msg);
    }

    public static void d(final String msg) {

        final Throwable t = new Throwable();
        final StackTraceElement[] elements = t.getStackTrace();

        final String callerClassName = elements[1].getClassName();
        final String callerMethodName = elements[1].getMethodName();

        Log.d(callerClassName, "[" + callerMethodName + "] " + msg);
    }

    public static void w(final String msg) {

        final Throwable t = new Throwable();
        final StackTraceElement[] elements = t.getStackTrace();

        final String callerClassName = elements[1].getClassName();
        final String callerMethodName = elements[1].getMethodName();

        Log.w(callerClassName, "[" + callerMethodName + "] " + msg);
    }
}
package com.sccci.sccci2014.util;

/**
 * Created by h3r0 on 6/21/14.
 */
public class Constants {

    public static String RETURN_SUCCESS = "success";

    public static String RETURN_FAIL = "fail";

    public static String URL_INFO = "http://sccci.herokuapp.com/api/infos";
    public static String URL_SPEAKER = "http://sccci.herokuapp.com/api/speakers";
    public static String URL_SPONSOR = "http://sccci.herokuapp.com/api/sponsors";
    public static String URL_EXHIBITOR = "http://sccci.herokuapp.com/api/exhibitors";
    public static String URL_GPE = "http://sccci.herokuapp.com/api/gov";
    public static String URL_PROGRAMME = "http://sccci.herokuapp.com/api/programs";
    public static String URL_PROMO = "http://sccci.herokuapp.com/api/promotions";
    public static String URL_NEWS = "http://sccci.herokuapp.com/api/news";
    public static String URL_CLINIC = "http://sccci.herokuapp.com/api/partners";

    public static String URL_POLL = "http://sccci.herokuapp.com/api/poll";
    public static String URL_LUCKYDRAW = "http://sccci.herokuapp.com/api/luckydraw";
    public static String URL_SURVEY = "http://sccci.herokuapp.com/api/survey";

    /*
    public static String URL_INFO = "http://192.168.200.135/sccci/info.json";
    public static String URL_SPEAKER = "http://192.168.200.135/sccci/speaker.json";
    public static String URL_SPONSOR = "http://192.168.200.135/sccci/organiser.json";
    public static String URL_EXHIBITOR = "http://192.168.200.135/sccci/exhibitor.json";
    public static String URL_PROGRAMME = "http://192.168.200.135/sccci/programme.json";
    */
}

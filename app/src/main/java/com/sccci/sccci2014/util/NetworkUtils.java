package com.sccci.sccci2014.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by h3r0 on 6/2/14.
 */
public class NetworkUtils {

    /**
     * Is device connected to network (WiFi or mobile).<br>
     * <br>
     * <b>Hint</b>: A connection to WiFi does not guarantee Internet access.
     *
     * @param context
     *
     * @return {@code true} if device is connected to mobile network or WiFi
     */
    public static boolean isConnected(Context context) {
        return isWiFiConnected(context) || isMobileNetworkConnected(context);
    }

    /**
     * Is device connected to WiFi?<br>
     * <br>
     * <b>Hint</b>: A connection to WiFi does not guarantee Internet access.
     *
     * @param context
     *
     * @return {@code true} if device is connected to an access point
     */
    public static boolean isWiFiConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    /**
     * Is device connected to mobile network?
     *
     * @param context
     *
     * @return {@code true} if device is connected to mobile network
     */
    public static boolean isMobileNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
    }
}

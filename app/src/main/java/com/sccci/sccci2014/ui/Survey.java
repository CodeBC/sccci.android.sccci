package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.webkit.WebView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.util.Constants;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Survey extends BaseActivity {

    @InjectView(R.id.webview)
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        ButterKnife.inject(this);

        setTitle();

        generateTopBanner();

        if(_appPrefs.isLangMandarin())
            webview.loadUrl(Constants.URL_SURVEY + "/language/cn");
        else
            webview.loadUrl(Constants.URL_SURVEY + "/language/en");
    }
}

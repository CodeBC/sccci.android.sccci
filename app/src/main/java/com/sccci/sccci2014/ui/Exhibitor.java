package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.ExhibitorAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjExhibitor;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Exhibitor extends BaseActivity {

    ArrayList<ObjExhibitor> objects = new ArrayList<ObjExhibitor>();

    @InjectView(R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibitor);
        ButterKnife.inject(this);

        setTitle();

        generateTopBanner();
        generateExhibitor();
    }

    private void generateExhibitor(){

        ObjExhibitor exb = new ObjExhibitor();
        exb.exhibitor_type = "Exhibitors";

        ObjExhibitor gpe = new ObjExhibitor();
        gpe.exhibitor_type = "Government Pavilion Exhibition";

        ObjExhibitor listall = new ObjExhibitor();
        listall.exhibitor_type = "List of All Exhibitors";

        ObjExhibitor floorplan = new ObjExhibitor();
        floorplan.exhibitor_type = "Exhibition Floorplan";

        objects.add(exb);
        objects.add(gpe);
        objects.add(listall);
        objects.add(floorplan);

        ExhibitorAdapter adapter = new ExhibitorAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);

    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {

        if(objects.get(position).exhibitor_type.equalsIgnoreCase("Exhibition Floorplan")){
            Intent floor = new Intent(this,ExhibitorFloorPlan.class);
            startActivity(floor);
        }else{
            Intent detail = new Intent(this, ExhibitorList.class);
            detail.putExtra("object",objects.get(position));
            startActivity(detail);
        }
    }
}

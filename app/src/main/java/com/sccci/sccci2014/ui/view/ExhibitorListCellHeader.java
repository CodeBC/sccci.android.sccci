package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.model.ObjExhibitor;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorListCellHeader {

    @InjectView(R.id.title)
    TextView name;

    public ExhibitorListCellHeader(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjExhibitor object) {

        name.setText("");

        if (object.exhibitor_type.equalsIgnoreCase("Sponsor")) {
            name.setText(object.exhibitor_category);
            return;
        }

        name.setText(object.exhibitor_name.subSequence(0, 1).charAt(0) + "");
    }
}

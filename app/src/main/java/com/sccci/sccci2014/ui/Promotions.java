package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.PromotionAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjPromotion;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Promotions extends BaseActivity {

    private Dao<ObjPromotion, Integer> promoDAO;

    ArrayList<ObjPromotion> objects = new ArrayList<ObjPromotion>();

    @InjectView(R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);
        ButterKnife.inject(this);

        try {
            promoDAO = OpenHelperManager.getHelper(this, DatabaseHelper.class).getPromoDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        generateTopBanner();
        generateInfo();
    }

    private void generateInfo() {

        try {
            objects = (ArrayList<ObjPromotion>) promoDAO.queryBuilder()
                    .orderBy("promotion_id", true)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        PromotionAdapter adapter = new PromotionAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);
    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {
        Intent detail = new Intent(this, PromotionDetail.class);
        detail.putExtra("object",objects.get(position));
        startActivity(detail);
    }
}

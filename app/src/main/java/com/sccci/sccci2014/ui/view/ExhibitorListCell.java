package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseCell;
import com.sccci.sccci2014.model.ObjExhibitor;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorListCell extends BaseCell{

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.avatar)
    ImageView avatar;

    public ExhibitorListCell(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjExhibitor object) {

        if(getPref().isLangMandarin())
            name.setText(object.exhibitor_name);
        else
            name.setText(object.exhibitor_name_cn);

        Picasso.with(getContext())
                .load(object.exhibitor_logo)
                .into(avatar);
    }
}

package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjEvent;
import com.sccci.sccci2014.model.ObjSpeaker;
import com.sccci.sccci2014.ui.view.ProgrammeListCell;
import com.sccci.sccci2014.ui.view.ProgrammeListHeader;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SpeakerDetail extends BaseActivity {

    @InjectView(R.id.avater)
    ImageView avater;

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.company)
    TextView company;

    @InjectView(R.id.designation)
    TextView designation;

    @InjectView(R.id.description)
    TextView description;

    @InjectView(R.id.content)
    LinearLayout content;

    ObjSpeaker object;

    Dao<ObjEvent, Integer> dao;

    boolean day1 = true, day2 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_detail);
        ButterKnife.inject(this);

        generateTopBanner();

        try {
            dao = OpenHelperManager.getHelper(this, DatabaseHelper.class).getProgrammerDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        object = (ObjSpeaker) getIntent().getExtras().get("object");

        getSupportActionBar().setTitle(object.speaker_name);

        Picasso.with(this)
                .load(object.speaker_avatar)
                .into(avater);

        if (_appPrefs.isLangMandarin()) {
            name.setText(object.speaker_name_cn);
            description.setText(Html.fromHtml(object.speaker_description_cn));
            company.setText(object.speaker_company_cn);
            designation.setText(object.speaker_designation_cn);

        } else {
            name.setText(object.speaker_name);
            description.setText(Html.fromHtml(object.speaker_description));
            company.setText(object.speaker_company);
            designation.setText(object.speaker_designation);
        }

        ArrayList<ObjEvent> objects = new ArrayList<ObjEvent>();

        try {
            objects = (ArrayList<ObjEvent>) dao.queryBuilder()
                    .orderBy("event_id", true)
                    .orderBy("time_name", false)
                    .orderBy("event_name", true)
                    .orderBy("start_time", true)
                    .orderBy("day_name", true)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < objects.size(); i++) {

            for (int j = 0; j < objects.get(i).speakers.size(); j++) {
                if (object.speaker_id == objects.get(i).speakers.get(j)) {

                    if (objects.get(i).day_name.equalsIgnoreCase("Wed 20 Aug") && day1) {
                        View convertView = getLayoutInflater().inflate(R.layout.listitem_programme_title, null, false);
                        ProgrammeListHeader holder = new ProgrammeListHeader(convertView);
                        holder.bind(((ObjEvent) objects.get(i)).day_name);
                        content.addView(convertView);
                        day1 = false;
                    }

                    if (objects.get(i).day_name.equalsIgnoreCase("Thu 21 Aug") && day2) {
                        View convertView = getLayoutInflater().inflate(R.layout.listitem_programme_title, null, false);
                        ProgrammeListHeader holder = new ProgrammeListHeader(convertView);
                        holder.bind(((ObjEvent) objects.get(i)).day_name);
                        content.addView(convertView);
                        day2 = false;
                    }

                    View convertView = getLayoutInflater().inflate(R.layout.listitem_programme, null, false);
                    ProgrammeListCell holder = new ProgrammeListCell(convertView);
                    holder.setContext(this);
                    holder.bind(objects.get(i), dao);

                    content.addView(convertView);
                }
            }

        }

    }

}

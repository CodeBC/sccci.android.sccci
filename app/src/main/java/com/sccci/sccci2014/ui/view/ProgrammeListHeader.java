package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.model.ObjEvent;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ProgrammeListHeader {

    @InjectView(R.id.title)
    TextView name;

    public ProgrammeListHeader(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjEvent object) {
        name.setText(object.time_name);
    }

    public void bind(String title) {
        name.setText(title);
    }
}

package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.webkit.WebView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class PartnerClinic extends BaseActivity {

    @InjectView(R.id.webview)
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_livepoll);
        ButterKnife.inject(this);

        generateTopBanner();

        String data = "";

        if(_appPrefs.isLangMandarin())
            data = _appPrefs.getPCCN();
        else
            data = _appPrefs.getPC();

        webview.loadDataWithBaseURL("file:///android_asset/", data, "text/html", "utf-8", null);
    }
}

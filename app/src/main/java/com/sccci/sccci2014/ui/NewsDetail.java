package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjNews;

import java.io.IOException;
import java.io.InputStream;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class NewsDetail extends BaseActivity {

    @InjectView(R.id.webview)
    WebView webview;

    ObjNews object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_detail);
        ButterKnife.inject(this);

        generateTopBanner();

        object = (ObjNews) getIntent().getExtras().get("object");

        getSupportActionBar().setTitle(object.news_title);

        try {
            webview.setWebViewClient(new WebViewClient());
            InputStream is = getResources().getAssets().open("about_template.html");
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String data = new String(buffer);

            if(_appPrefs.isLangMandarin()){
                data = data.replaceAll("<!-- TITLE -->", object.news_title_cn);
                data = data.replaceAll("<!-- CONTENT -->", object.news_description_cn);
            }else{
                data = data.replaceAll("<!-- TITLE -->", object.news_title);
                data = data.replaceAll("<!-- CONTENT -->", object.news_description);
            }

            webview.loadDataWithBaseURL("file:///android_asset/", data, "text/html", "utf-8", null);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.SponsorAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjSponsor;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnItemClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Sponsors extends BaseActivity {

    private Dao<ObjSponsor,Integer> sponsorDao;

    ArrayList<ObjSponsor> objects = new ArrayList<ObjSponsor>();

    @InjectView(R.id.list)
    StickyListHeadersListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);
        //ButterKnife.inject(this);

        setTitle();

        list = (StickyListHeadersListView) findViewById(R.id.list);
        top_banner = (ViewPager) findViewById(R.id.top_banner);

        try {
            sponsorDao = OpenHelperManager.getHelper(this, DatabaseHelper.class).getSponsorDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        generateTopBanner();
        generateSponsors();
    }

    private void generateSponsors(){

        try {
            if(_appPrefs.isLangMandarin())
                objects = (ArrayList<ObjSponsor>) sponsorDao.queryBuilder()
                        .orderBy("exhibitor_category", false)
                        .orderBy("sponsor_name_cn", true)
                        .query();
            else
                objects = (ArrayList<ObjSponsor>) sponsorDao.queryBuilder()
                        .orderBy("exhibitor_category", false)
                        .orderBy("exhibitor_name", true)
                        .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        SponsorAdapter adapter = new SponsorAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OnItemClicked(position);
            }
        });
    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {
        Intent detail = new Intent(this, SponsorDetail.class);
        detail.putExtra("object",objects.get(position));
        startActivity(detail);
    }
}

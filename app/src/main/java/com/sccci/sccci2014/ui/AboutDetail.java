package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjAbout;

import java.io.IOException;
import java.io.InputStream;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class AboutDetail extends BaseActivity {

    @InjectView(R.id.webview)
    WebView webview;

    ObjAbout object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_detail);
        ButterKnife.inject(this);

        generateTopBanner();

        object = (ObjAbout) getIntent().getExtras().get("object");

        if(_appPrefs.isLangMandarin())
            getSupportActionBar().setTitle(object.title_cn);
        else
            getSupportActionBar().setTitle(object.title);

        try {
            webview.setWebViewClient(new WebViewClient());
            InputStream is = getResources().getAssets().open("about_template.html");
            int size = is.available();

            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String data = new String(buffer);

            if(_appPrefs.isLangMandarin()){
                data = data.replaceAll("<!-- TITLE -->", object.title_cn);
                data = data.replaceAll("<!-- CONTENT -->", object.description_cn);
            }else{
                data = data.replaceAll("<!-- TITLE -->", object.title);
                data = data.replaceAll("<!-- CONTENT -->", object.description);
            }

            webview.loadDataWithBaseURL("file:///android_asset/", data, "text/html", "utf-8", null);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_share:
                ShareViaFB();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}

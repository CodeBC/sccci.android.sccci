package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseCell;
import com.sccci.sccci2014.model.ObjSpeaker;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SpeakerListCell extends BaseCell{

    @InjectView(R.id.name)
    TextView name;

    public SpeakerListCell(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjSpeaker object) {

        if(getPref().isLangMandarin())
            name.setText(object.speaker_name_cn);
        else
            name.setText(object.speaker_name);
    }
}

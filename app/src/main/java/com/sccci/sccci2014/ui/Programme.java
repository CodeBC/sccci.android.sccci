package com.sccci.sccci2014.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.ProgrammeFragmentAdapter;
import com.sccci.sccci2014.core.BaseFragmentActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.GridItem;
import com.sccci.sccci2014.model.ObjEvent;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Programme extends BaseFragmentActivity {

    @InjectView(R.id.pager)
    ViewPager viewPager;

    @InjectView(R.id.tabs)
    PagerSlidingTabStrip tabs;

    GridItem object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programme);
        ButterKnife.inject(this);

        object = (GridItem) getIntent().getExtras().get("object");

        if(_appPrefs.isLangMandarin())
            getActionBar().setTitle(object.cn_name);
        else
            getActionBar().setTitle(object.en_name);

        ProgrammeFragmentAdapter adapter;

        if(getDays().isEmpty()){
            viewPager.setVisibility(ViewPager.GONE);
            tabs.setVisibility(PagerSlidingTabStrip.GONE);
        }else{
            if(object.en_name.equalsIgnoreCase("Programme"))
                adapter = new ProgrammeFragmentAdapter(getSupportFragmentManager(),getDays());
            else
                adapter = new ProgrammeFragmentAdapter(getSupportFragmentManager(),getDays(),true);

            viewPager.setAdapter(adapter);
            tabs.setViewPager(viewPager);
            tabs.setTextColor(Color.BLACK);
            tabs.setIndicatorColor(getResources().getColor(R.color.blueish));
            tabs.setBackgroundColor(Color.WHITE);
        }

        generateTopBanner();
    }

    ArrayList<ObjEvent> getDays(){

        ArrayList<ObjEvent> objects = new ArrayList<ObjEvent>();
        Dao<ObjEvent, Integer> dao;

        try {
            dao = OpenHelperManager.getHelper(this, DatabaseHelper.class).getProgrammerDAO();

            if(object.en_name.equalsIgnoreCase("Programme")){
                objects = (ArrayList<ObjEvent>) dao.queryBuilder()
                        .orderBy("event_id", true)
                        .groupBy("day_name")
                        .query();
            }else{
                objects = (ArrayList<ObjEvent>) dao.queryBuilder()
                        .orderBy("event_id", true)
                        .groupBy("day_name")
                        .where().eq("is_saved",true)
                        .query();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return objects;
    }

}
package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjExhibitor;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorDetail extends BaseActivity {

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.booth)
    TextView booth;

    @InjectView(R.id.contact)
    TextView contact;

    @InjectView(R.id.about)
    TextView about;

    @InjectView(R.id.avater)
    ImageView avater;

    ObjExhibitor object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibitor_detail);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        generateTopBanner();

        object = (ObjExhibitor) getIntent().getExtras().get("object");

        if(_appPrefs.isLangMandarin())
            getSupportActionBar().setTitle(object.exhibitor_name_cn);
        else
            getSupportActionBar().setTitle(object.exhibitor_name);

        Picasso.with(this)
                .load(object.exhibitor_logo)
                .into(avater);

        booth.setText(object.exhibitor_booth);

        if(_appPrefs.isLangMandarin()){
            name.setText(object.exhibitor_name_cn);
            contact.setText(Html.fromHtml(object.exhibitor_contact_cn));
            about.setText(Html.fromHtml(object.exhibitor_about_cn));
        }else{
            name.setText(object.exhibitor_name);
            contact.setText(Html.fromHtml(object.exhibitor_contact));
            about.setText(Html.fromHtml(object.exhibitor_about));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

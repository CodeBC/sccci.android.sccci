package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseCell;
import com.sccci.sccci2014.model.ObjSponsor;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SponsorListCell extends BaseCell{

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.booth)
    TextView booth;

    @InjectView(R.id.avatar)
    ImageView avatar;

    public SponsorListCell(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjSponsor object) {

        if(getPref().isLangMandarin())
            name.setText(object.sponsor_name_cn);
        else
            name.setText(object.exhibitor_name);

        Picasso.with(getContext())
                .load(object.exhibitor_logo)
                .into(avatar);

        booth.setText(object.exhibitor_booth);
    }
}

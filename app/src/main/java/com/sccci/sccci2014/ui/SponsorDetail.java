package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjSponsor;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SponsorDetail extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.booth)
    TextView booth;

    @InjectView(R.id.logo)
    ImageView logo;

    @InjectView(R.id.contact)
    TextView contact;

    @InjectView(R.id.about)
    TextView about;

    ObjSponsor object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsor_detail);
        ButterKnife.inject(this);

        generateTopBanner();

        object = (ObjSponsor) getIntent().getExtras().get("object");

        getSupportActionBar().setTitle(object.exhibitor_name);

        Picasso.with(this)
                .load(object.exhibitor_logo)
                .into(logo);

        booth.setText(object.exhibitor_booth);

        if(_appPrefs.isLangMandarin()){
            title.setText(object.sponsor_name_cn);
            about.setText(Html.fromHtml(object.sponsor_about_cn));
            contact.setText(Html.fromHtml(object.exhibitor_contact_cn));
        }else{
            title.setText(object.exhibitor_name);
            about.setText(Html.fromHtml(object.exhibitor_about));
            contact.setText(Html.fromHtml(object.exhibitor_contact));
        }

    }

}

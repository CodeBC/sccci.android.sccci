package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseCell;
import com.sccci.sccci2014.model.GridItem;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class GridCell extends BaseCell{

    @InjectView(R.id.thumb)
    ImageView thumb;
    @InjectView(R.id.name)
    TextView name;

    public GridCell(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(GridItem object) {
        thumb.setImageResource(object.thumb);

        if(getPref().isLangMandarin())
            name.setText(object.cn_name);
        else
            name.setText(object.en_name);
    }
}

package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AdapterView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.ExhibitorListAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjExhibitor;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnItemClick;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorList extends BaseActivity {

    private Dao<ObjExhibitor, Integer> exhibitorDAO;

    ArrayList<ObjExhibitor> objects = new ArrayList<ObjExhibitor>();

    ObjExhibitor object;

    @InjectView(R.id.list)
    StickyListHeadersListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibitor_list);
        //ButterKnife.inject(this);

        list = (StickyListHeadersListView) findViewById(R.id.list);
        top_banner = (ViewPager) findViewById(R.id.top_banner);

        object = (ObjExhibitor) getIntent().getExtras().get("object");

        getSupportActionBar().setTitle(object.exhibitor_type);

        try {
            exhibitorDAO = OpenHelperManager.getHelper(this, DatabaseHelper.class).getExhibitorDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        generateTopBanner();
        generateExhibitor();
    }

    private void generateExhibitor() {

        try {

            if(object.exhibitor_type.equalsIgnoreCase("List of All Exhibitors")){
                objects = (ArrayList<ObjExhibitor>) exhibitorDAO.queryBuilder()
                        .orderBy("exhibitor_category", true)
                        .orderBy("exhibitor_name", true)
                        .query();
            }else{
                objects = (ArrayList<ObjExhibitor>) exhibitorDAO.queryBuilder()
                        .orderBy("exhibitor_category", true)
                        .orderBy("exhibitor_name", true)
                        .where().eq("exhibitor_type", object.exhibitor_type)
                        .query();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        ExhibitorListAdapter adapter = new ExhibitorListAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OnItemClicked(position);
            }
        });
    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {
        Intent detail = new Intent(this, ExhibitorDetail.class);
        detail.putExtra("object", objects.get(position));
        startActivity(detail);
    }
}

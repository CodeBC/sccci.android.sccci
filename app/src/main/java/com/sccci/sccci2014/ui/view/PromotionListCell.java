package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseCell;
import com.sccci.sccci2014.model.ObjPromotion;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class PromotionListCell extends BaseCell{

    @InjectView(R.id.name)
    TextView name;

    public PromotionListCell(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjPromotion object) {
        if(getPref().isLangMandarin())
            name.setText(object.promotion_title);
        else
            name.setText(object.promotion_title_cn);
    }
}

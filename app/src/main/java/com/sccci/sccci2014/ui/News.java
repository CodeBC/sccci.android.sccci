package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.NewsAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjNews;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * Created by h3r0 on 6/18/14.
 */
public class News extends BaseActivity {

    private Dao<ObjNews, Integer> newsDAO;

    ArrayList<ObjNews> objects = new ArrayList<ObjNews>();

    @InjectView(R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.inject(this);

        try {
            newsDAO = OpenHelperManager.getHelper(this, DatabaseHelper.class).getNewsDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        generateTopBanner();
        generateInfo();
    }

    private void generateInfo() {

        try {
            objects = (ArrayList<ObjNews>) newsDAO.queryBuilder()
                    .orderBy("news_id", true)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        NewsAdapter adapter = new NewsAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);
    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {
        Intent detail = new Intent(this, NewsDetail.class);
        detail.putExtra("object",objects.get(position));
        startActivity(detail);
    }
}

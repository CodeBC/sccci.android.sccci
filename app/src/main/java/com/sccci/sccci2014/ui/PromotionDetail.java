package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjPromotion;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class PromotionDetail extends BaseActivity {

    @InjectView(R.id.logo)
    ImageView logo;

    @InjectView(R.id.detail)
    TextView detail;

    ObjPromotion object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_detail);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        object = (ObjPromotion) getIntent().getExtras().get("object");

        getSupportActionBar().setTitle(object.promotion_title);

        Picasso.with(this)
                .load(object.promotion_logo)
                .into(logo);

        detail.setText(Html.fromHtml(object.promotion_description));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_share:
                ShareViaFB();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.share, menu);
        return super.onCreateOptionsMenu(menu);
    }
}

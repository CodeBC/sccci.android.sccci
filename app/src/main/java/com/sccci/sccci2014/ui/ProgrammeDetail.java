package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjEvent;
import com.sccci.sccci2014.model.ObjSpeaker;

import java.sql.SQLException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ProgrammeDetail extends BaseActivity {

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.description)
    TextView description;

    @InjectView(R.id.time_venue)
    TextView time_venue;

    @InjectView(R.id.speakers)
    TextView speakers;

    ObjEvent object;

    private Dao<ObjSpeaker,Integer> speakerDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programme_detail);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        try {
            speakerDAO = OpenHelperManager.getHelper(this, DatabaseHelper.class).getSpeakerDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        generateTopBanner();

        object = (ObjEvent) getIntent().getExtras().get("object");

        getSupportActionBar().setTitle(object.event_name);

        if(_appPrefs.isLangMandarin()){
            title.setText(object.event_name_cn);
            description.setText(Html.fromHtml(object.event_description_cn));
        }else{
            title.setText(object.event_name);
            description.setText(Html.fromHtml(object.event_description));
        }

        time_venue.setText( object.event_location + "   " + object.start_time + " to " + object.end_time + "   " + object.event_language);

        String speakerlist = "";
        for(int i=0;i<object.speakers.size();i++){

            try {
                List<ObjSpeaker> spk = speakerDAO.queryBuilder()
                        .where().eq("speaker_id", object.speakers.get(i))
                        .query();

                if(_appPrefs.isLangMandarin())
                    speakerlist += "<p>" + spk.get(0).speaker_name_cn + "</p>";
                else
                    speakerlist += "<p>" + spk.get(0).speaker_name + "</p>";

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        speakers.setText(Html.fromHtml(speakerlist));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_share:
                ShareViaFB();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.share, menu);
        return super.onCreateOptionsMenu(menu);
    }
}

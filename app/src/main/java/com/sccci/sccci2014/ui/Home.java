package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.HomeGridAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.GridItem;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Home extends BaseActivity {

    @InjectView(R.id.grid)
    GridView grid;

    private ArrayList<GridItem> objects = new ArrayList<GridItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        generateMenu();
        generateTopBanner();
    }

    private void generateMenu() {

        GridItem about = new GridItem(getResources().getString(R.string.screen_about), getResources().getString(R.string.screen_about_cn), R.drawable.about_icon, About.class);
        GridItem programme = new GridItem(getResources().getString(R.string.screen_programme), getResources().getString(R.string.screen_programme_cn), R.drawable.programs_icon, Programme.class);
        GridItem speakers = new GridItem(getResources().getString(R.string.screen_speakers), getResources().getString(R.string.screen_speakers_cn), R.drawable.speakers_icon, Speakers.class);
        GridItem map = new GridItem(getResources().getString(R.string.screen_map), getResources().getString(R.string.screen_map_cn), R.drawable.map_icon, Map.class);
        GridItem schedule = new GridItem("My Schedule", "我的日程", R.drawable.my_schedule, Programme.class);
        GridItem exhibitor = new GridItem(getResources().getString(R.string.screen_exhibitor), getResources().getString(R.string.screen_exhibitor_cn), R.drawable.exhibitor_icon, Exhibitor.class);
        GridItem organisers = new GridItem(getResources().getString(R.string.screen_sponsors), getResources().getString(R.string.screen_sponsors_cn), R.drawable.sponsor_icon, Sponsors.class);
        GridItem highlights = new GridItem(getResources().getString(R.string.screen_highlights), getResources().getString(R.string.screen_highlights_cn), R.drawable.highlight_icon, Highlights.class);
        GridItem luckydraw = new GridItem(getResources().getString(R.string.screen_luckydraw), getResources().getString(R.string.screen_luckydraw_cn), R.drawable.lucky_draw_icon, LuckyDraw.class);
        GridItem livepoll = new GridItem(getResources().getString(R.string.screen_livepoll), getResources().getString(R.string.screen_livepoll_cn), R.drawable.live_poll_icon, LivePoll.class);
        GridItem survey = new GridItem(getResources().getString(R.string.screen_survey), getResources().getString(R.string.screen_survey_cn), R.drawable.survey_icon, Survey.class);
        GridItem setting = new GridItem(getResources().getString(R.string.screen_setting), getResources().getString(R.string.screen_setting_cn), R.drawable.setting_icon, Setting.class);

        objects.clear();

        objects.add(about);
        objects.add(programme);
        objects.add(speakers);
        objects.add(map);
        objects.add(schedule);
        objects.add(exhibitor);
        objects.add(organisers);
        objects.add(highlights);
        //objects.add(luckydraw);
        //objects.add(livepoll);
        //objects.add(survey);
        objects.add(setting);

        HomeGridAdapter adapter = new HomeGridAdapter(getApplicationContext());
        adapter.setData(objects);
        grid.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.language, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_lang:

                Intent go = new Intent(this, Setting.class);
                go.putExtra("object", objects.get(objects.size()-1));
                startActivity(go);

                break;
            case android.R.id.home:

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnItemClick(R.id.grid)
    void OnItemClicked(int position) {
        Intent go = new Intent(this, objects.get(position).cls);
        go.putExtra("object", objects.get(position));
        startActivity(go);
    }

    @Override
    public void onResume() {
        super.onResume();
        generateMenu();
    }
}

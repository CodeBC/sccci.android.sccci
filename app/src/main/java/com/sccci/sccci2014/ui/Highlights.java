package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.AboutAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.model.ObjAbout;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Highlights extends BaseActivity {

    ArrayList<ObjAbout> objects = new ArrayList<ObjAbout>();

    @InjectView(R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.inject(this);

        setTitle();

        generateTopBanner();
        generateInfo();
    }

    private void generateInfo() {

        ObjAbout promo = new ObjAbout();
        promo.title = "Special Promotions";
        promo.title_cn = "Special Promotions";

        ObjAbout partner = new ObjAbout();
        partner.title = "Partner's Clinic";
        partner.title_cn = "Partner's Clinic";

        ObjAbout news = new ObjAbout();
        news.title = "Other News";
        news.title_cn = "Other News";

        objects.add(promo);
        objects.add(partner);
        objects.add(news);

        AboutAdapter adapter = new AboutAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);
    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {
        switch (position){
            case 0:
                Intent promo = new Intent(this,Promotions.class);
                startActivity(promo);
                break;
            case 1:
                Intent partner = new Intent(this,PartnerClinic.class);
                startActivity(partner);
                break;
            case 2:
                Intent news = new Intent(this,News.class);
                startActivity(news);
                break;
        }
    }
}

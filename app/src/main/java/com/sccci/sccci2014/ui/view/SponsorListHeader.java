package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.TextView;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.model.ObjSponsor;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class SponsorListHeader {

    @InjectView(R.id.title)
    TextView name;

    public SponsorListHeader(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(ObjSponsor object) {

        name.setText(object.exhibitor_category);

    }
}

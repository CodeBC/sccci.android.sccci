package com.sccci.sccci2014.ui;

import android.os.Bundle;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseActivity;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ExhibitorFloorPlan extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibitor_floorplan);
    }
}

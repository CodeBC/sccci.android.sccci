package com.sccci.sccci2014.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.adapter.SpeakerAdapter;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.db.DatabaseHelper;
import com.sccci.sccci2014.model.ObjSpeaker;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Speakers extends BaseActivity {

    private Dao<ObjSpeaker,Integer> speakerDAO;

    ArrayList<ObjSpeaker> objects = new ArrayList<ObjSpeaker>();

    @InjectView(R.id.list)
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker);
        ButterKnife.inject(this);

        setTitle();

        try {
            speakerDAO = OpenHelperManager.getHelper(this, DatabaseHelper.class).getSpeakerDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        generateTopBanner();
        generateSpeaker();
    }

    private void generateSpeaker() {

        try {
            if(_appPrefs.isLangMandarin())
                objects = (ArrayList<ObjSpeaker>) speakerDAO.queryBuilder()
                        .orderBy("speaker_name_cn", true)
                        .query();
            else
                objects = (ArrayList<ObjSpeaker>) speakerDAO.queryBuilder()
                        .orderBy("speaker_name", true)
                        .query();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        SpeakerAdapter adapter = new SpeakerAdapter(this);
        adapter.setData(objects);
        list.setAdapter(adapter);
    }

    @OnItemClick(R.id.list)
    void OnItemClicked(int position) {
        Intent detail = new Intent(this, SpeakerDetail.class);
        detail.putExtra("object",objects.get(position));
        startActivity(detail);
    }
}

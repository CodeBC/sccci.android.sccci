package com.sccci.sccci2014.ui.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.sccci.sccci2014.R;
import com.sccci.sccci2014.core.BaseCell;
import com.sccci.sccci2014.model.ObjEvent;

import java.sql.SQLException;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class ProgrammeListCell extends BaseCell{

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.time_venue)
    TextView time_venue;

    @InjectView(R.id.status_indicator)
    ImageView status_indicator;

    public ProgrammeListCell(View view) {
        ButterKnife.inject(this, view);
    }

    public void bind(final ObjEvent object, final Dao<ObjEvent, Integer> dao) {

        if(getPref().isLangMandarin())
            name.setText(object.event_name_cn);
        else
            name.setText(object.event_name);

        time_venue.setText( object.event_location + "   " + object.start_time + " to " + object.end_time + "   " + object.event_language);

        if(object.is_saved)
            status_indicator.setImageResource(R.drawable.programme_cell_icon_active);
        else
            status_indicator.setImageResource(R.drawable.programme_cell_icon_deactive);

        status_indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(object.is_saved){
                    object.is_saved = false;
                    status_indicator.setImageResource(R.drawable.programme_cell_icon_deactive);

                    try {
                        dao.update(object);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    return;
                }

                if(!object.is_saved){
                    object.is_saved = true;
                    status_indicator.setImageResource(R.drawable.programme_cell_icon_active);

                    try {
                        dao.update(object);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    return;
                }
            }
        });
    }
}

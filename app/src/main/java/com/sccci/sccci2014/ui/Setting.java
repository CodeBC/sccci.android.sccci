package com.sccci.sccci2014.ui;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.sccci.sccci2014.R;
import com.sccci.sccci2014.api.AboutLoader;
import com.sccci.sccci2014.api.ClinicLoader;
import com.sccci.sccci2014.api.ExhibitorLoader;
import com.sccci.sccci2014.api.NewsLoader;
import com.sccci.sccci2014.api.ProgrammeLoader;
import com.sccci.sccci2014.api.PromotionLoader;
import com.sccci.sccci2014.api.SpeakerLoader;
import com.sccci.sccci2014.api.SponsorLoader;
import com.sccci.sccci2014.core.BaseActivity;
import com.sccci.sccci2014.util.Constants;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by h3r0 on 6/18/14.
 */
public class Setting extends BaseActivity implements LoaderManager.LoaderCallbacks<String>  {

    @InjectView(R.id.push_notification)
    Switch push_notification;

    @InjectView(R.id.lang)
    Switch lang;

    @InjectView(R.id.sync)
    Button sync;

    @InjectView(R.id.pg_sync)
    ProgressBar pg_sync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.inject(this);

        generateTopBanner();

        init_lang();
        init_notification();
        init_sync();

        setTitle();

        updateLanguage();
    }

    void updateLanguage() {

        push_notification.setText("Push Notification (通知) ");
        lang.setText("Language (语言)");

        /*
        if(_appPrefs.isLangMandarin()){
            push_notification.setText("通知");
            lang.setText("语言");
        }else{
            push_notification.setText("Push Notification (通知) ");
            lang.setText("Language (语言)");
        }
        */
    }

    void init_sync(){
        sync.setVisibility(Button.GONE);
        pg_sync.setVisibility(ProgressBar.GONE);

        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sync.setVisibility(Button.GONE);
                pg_sync.setVisibility(ProgressBar.VISIBLE);

                _appPrefs.setAboutLoaded(false);
                _appPrefs.setClinicLoaded(false);
                _appPrefs.setExhibitorsLoaded(false);
                _appPrefs.setNewsLoaded(false);
                _appPrefs.setProgrammeLoaded(false);
                _appPrefs.setPromotionLoaded(false);
                _appPrefs.setSpeakersLoaded(false);
                _appPrefs.setSponsorsLoaded(false);

                getSupportLoaderManager().initLoader(0, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(1, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(2, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(3, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(4, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(5, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(6, null, Setting.this).forceLoad();
                getSupportLoaderManager().initLoader(7, null, Setting.this).forceLoad();
            }
        });
    }

    void init_notification() {

        push_notification.setChecked(_appPrefs.isNotificationEnabled());

        push_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                _appPrefs.setNotify(isChecked);
            }
        });
    }

    void init_lang() {

        lang.setChecked(_appPrefs.isLangMandarin());

        lang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                _appPrefs.setLang(isChecked);
                updateLanguage();
            }
        });
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        if (id == 0) return new AboutLoader(this);
        if (id == 1) return new ExhibitorLoader(this);
        if (id == 2) return new NewsLoader(this);
        if (id == 3) return new ProgrammeLoader(this);
        if (id == 4) return new PromotionLoader(this);
        if (id == 5) return new SpeakerLoader(this);
        if (id == 6) return new SponsorLoader(this);
        if (id == 7) return new ClinicLoader(this);
        return null;
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {

        if (loader.getId() == 0 && checkReturn(data)) _appPrefs.setAboutLoaded(true);
        if (loader.getId() == 1 && checkReturn(data)) _appPrefs.setExhibitorsLoaded(true);
        if (loader.getId() == 2 && checkReturn(data)) _appPrefs.setNewsLoaded(true);
        if (loader.getId() == 3 && checkReturn(data)) _appPrefs.setProgrammeLoaded(true);
        if (loader.getId() == 4 && checkReturn(data)) _appPrefs.setPromotionLoaded(true);
        if (loader.getId() == 5 && checkReturn(data)) _appPrefs.setSpeakersLoaded(true);
        if (loader.getId() == 6 && checkReturn(data)) _appPrefs.setSponsorsLoaded(true);
        if (loader.getId() == 7 && checkReturn(data)) _appPrefs.setClinicLoaded(true);

        if (_appPrefs.isAboutLoaded()
                && _appPrefs.isExhibitorsLoaded()
                && _appPrefs.isNewsLoaded()
                && _appPrefs.isProgrammeLoaded()
                && _appPrefs.isPromotionLoaded()
                && _appPrefs.isSpeakersLoaded()
                && _appPrefs.isSponsorsLoaded()
                && _appPrefs.isClinicLoaded()) {

            Toast.makeText(this,"Data Sync Done.",Toast.LENGTH_SHORT).show();
            sync.setVisibility(Button.GONE);
            pg_sync.setVisibility(ProgressBar.GONE);

        }
    }

    public boolean checkReturn(String data) {
        if (data.equalsIgnoreCase(Constants.RETURN_SUCCESS))
            return true;
        return true;
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }
}

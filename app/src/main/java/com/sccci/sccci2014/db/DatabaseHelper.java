package com.sccci.sccci2014.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sccci.sccci2014.model.ObjAbout;
import com.sccci.sccci2014.model.ObjEvent;
import com.sccci.sccci2014.model.ObjExhibitor;
import com.sccci.sccci2014.model.ObjNews;
import com.sccci.sccci2014.model.ObjSponsor;
import com.sccci.sccci2014.model.ObjPromotion;
import com.sccci.sccci2014.model.ObjSpeaker;

import java.sql.SQLException;

/**
 * Created by h3r0 on 6/21/14.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "sccci.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<ObjSpeaker, Integer> speakerDAO = null;
    private Dao<ObjAbout, Integer> infoDAO = null;
    private Dao<ObjPromotion, Integer> promoDAO = null;
    private Dao<ObjNews, Integer> newsDAO = null;
    private Dao<ObjSponsor, Integer> sponsorDAO = null;
    private Dao<ObjExhibitor, Integer> exhibitorDAO = null;
    private Dao<ObjEvent, Integer> programmeDAO = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, ObjSpeaker.class);
            TableUtils.createTable(connectionSource, ObjAbout.class);
            TableUtils.createTable(connectionSource, ObjNews.class);
            TableUtils.createTable(connectionSource, ObjPromotion.class);
            TableUtils.createTable(connectionSource, ObjSponsor.class);
            TableUtils.createTable(connectionSource, ObjExhibitor.class);
            TableUtils.createTable(connectionSource, ObjEvent.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public Dao<ObjSpeaker, Integer> getSpeakerDAO() throws SQLException {
        if (speakerDAO == null) {
            speakerDAO = getDao(ObjSpeaker.class);
        }
        return speakerDAO;
    }

    public Dao<ObjAbout, Integer> getInfoDAO() throws SQLException {
        if (infoDAO == null) {
            infoDAO = getDao(ObjAbout.class);
        }
        return infoDAO;
    }

    public Dao<ObjNews, Integer> getNewsDAO() throws SQLException {
        if (newsDAO == null) {
            newsDAO = getDao(ObjNews.class);
        }
        return newsDAO;
    }

    public Dao<ObjPromotion, Integer> getPromoDAO() throws SQLException {
        if (promoDAO == null) {
            promoDAO = getDao(ObjPromotion.class);
        }
        return promoDAO;
    }

    public Dao<ObjSponsor, Integer> getSponsorDAO() throws SQLException {
        if (sponsorDAO == null) {
            sponsorDAO = getDao(ObjSponsor.class);
        }
        return sponsorDAO;
    }

    public Dao<ObjExhibitor, Integer> getExhibitorDAO() throws SQLException {
        if (exhibitorDAO == null) {
            exhibitorDAO = getDao(ObjExhibitor.class);
        }
        return exhibitorDAO;
    }

    public Dao<ObjEvent, Integer> getProgrammerDAO() throws SQLException {
        if (programmeDAO == null) {
            programmeDAO = getDao(ObjEvent.class);
        }
        return programmeDAO;
    }

    @Override
    public void close() {
        super.close();

        speakerDAO = null;
        infoDAO = null;
        promoDAO = null;
        sponsorDAO = null;
        exhibitorDAO = null;
        programmeDAO = null;
    }
}
